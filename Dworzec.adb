-- Dworzec.adb
--
--

with Ada.Text_IO;
use Ada.Text_IO;
with Ada.Float_Text_IO;
use Ada.Float_Text_IO;

with Ada.Integer_Text_IO;
use Ada.Integer_Text_IO;
with Ada.Real_Time;
use Ada.Real_Time;

with Ada.Calendar;
use Ada.Calendar;

with Ada.Strings;
use Ada.Strings;
with Ada.Strings.Fixed;
use Ada.Strings.Fixed;

with Ada.Exceptions;
use Ada.Exceptions;

procedure Dworzec is
  
-- ------------------------------------
-- 		Zmienne globalne
-- ------------------------------------

Koniec : Boolean := False with Atomic;  	-- zmienna konca programu
Aktualna_Godzina : Integer :=0 with Atomic;	-- zmienna okreslajaca aktualna godzine
Aktualna_Minuta : Integer := 0 with Atomic;	-- zmienna okreslajaca aktualna minute
Godzina : Integer :=0;			-- zmienna do przypisania godziny odjazdu pociagu
Minuta : Integer :=0;			-- zmienna do przypisania minut odjazdu pociagu
Nazwa_pociagu : String(1..15) := (others => ' ');	-- tablica zmiennych przechowujaca nazwe pociagu
Dlugosc_nazwy_pociagu : Integer := 0;	-- zmienna okreslajaca dlugosc nazwy pociagu
Zn : Character;				-- zmienna do obslugi klawiatury
Nie_Rysuj : Boolean := False with Atomic;   -- zmienna do zawieszania rysowania w zadaniach
N : Natural := 4; 				-- zmienna okreslajaca ilosc peronow
Perony : array(1..N) of Boolean := (others => False);	-- tablica zmiennych okreslajaca zajetosc peronow

  -- ------------------------------------
  -- 		Procedury własne
  -- ------------------------------------

procedure Nowy_pociag_godz; 	-- procedura do przypisania godziny odjazdu pocaigu
procedure Nowy_pociag_min;	-- procedura do przypisania minut odjazdu pocaigu
procedure Nowy_pociag_naz;	-- procedura do przypisania nazwy pocaigu


  -- ------------------------------------
  -- 		Semafor liczbowy
  -- ------------------------------------
protected type Semafor_Liczbowy(Init_Sem: Natural := N) is
  entry Czekaj;
  procedure Sygnalizuj;
  private
  S : Natural := Init_Sem;
end Semafor_Liczbowy ;

protected body Semafor_Liczbowy  is
	entry Czekaj when S>0 is
    	begin
		S := S-1;
	end Czekaj;
	procedure Sygnalizuj  is
  	begin
    		S := S+1;
	end Sygnalizuj;
	
end Semafor_Liczbowy ;
  
Semafor1: Semafor_Liczbowy(N); -- inicjalizacja semafora
  -- ----------------------------------------
  -- Obiekt chroniony ekran do wyswietlania
  -- ----------------------------------------
type Atrybuty is (Czysty, Jasny, Podkreslony, Negatyw, Migajacy, Szary);
protected Ekran  is
	procedure Pisz_XY(X,Y: Positive; S: String; Atryb : Atrybuty := Czysty);
	procedure Pisz_Float_XY(X, Y: Positive; 
                            Num: Float; 
                            Pre: Natural := 3; 
                            Aft: Natural := 2; 
                            Exp: Natural := 0; 
                            Atryb : Atrybuty := Czysty);
	procedure Czysc; 			-- Procedura do czyszczenia ekranu
	procedure Tlo; 			-- Procedura do wyswietlania menu glownego
	procedure Nowy_pociag_godzina; 	-- Procedura od wyswietlania przypisania godziny
	procedure Nowy_pociag_minuta; 	--  Procedura od wyswietlania przypisania minut
	procedure Nowy_pociag_nazwa; 	--  Procedura od wyswietlania przypisania nazwy
end Ekran;
 
protected body Ekran is
-- implementacja dla Linuxa i macOSX
	function Atryb_Fun(Atryb : Atrybuty) return String is 
	(case Atryb is 
       	when Jasny => "1m", when Podkreslony => "4m", when Negatyw => "7m",
       	when Migajacy => "5m", when Szary => "2m", when Czysty => "0m"); 
       
   	function Esc_XY(X,Y : Positive) return String is 
      	( (ASCII.ESC & "[" & Trim(Y'Img,Both) & ";" & Trim(X'Img,Both) & "H") );   
       
    	procedure Pisz_XY(X,Y: Positive; S: String; Atryb : Atrybuty := Czysty) is
      	Przed : String := ASCII.ESC & "[" & Atryb_Fun(Atryb);              
    	begin
      		Put( Przed);
      		Put( Esc_XY(X,Y) & S);
      		Put( ASCII.ESC & "[0m");
    	end Pisz_XY;  
    
    	procedure Pisz_Float_XY(X, Y: Positive; 
                            Num: Float; 
                            Pre: Natural := 3; 
                            Aft: Natural := 2; 
                            Exp: Natural := 0; 
                            Atryb : Atrybuty := Czysty) is
                              
      		Przed_Str : String := ASCII.ESC & "[" & Atryb_Fun(Atryb);              
    	begin
      		Put( Przed_Str);
      		Put( Esc_XY(X, Y) );
      		Put( Num, Pre, Aft, Exp);
      		Put( ASCII.ESC & "[0m");
    	end Pisz_Float_XY; 
    
    	procedure Czysc is
    	begin
      		Put(ASCII.ESC & "[2J");
    	end Czysc;   
    

	procedure Tlo is
    	begin
      		Ekran.Czysc;
      		Ekran.Pisz_XY(1,1,"+=========== Dworzec kolejowy ===========+"); 
	      	Ekran.Pisz_XY(15,5, "---Peron 1---");
      		Ekran.Pisz_XY(3,6, "Tor 1: ");
      		Ekran.Pisz_XY(3,7, "Tor 2:");
      		Ekran.Pisz_XY(15,9, "---Peron 2---");
      		Ekran.Pisz_XY(3,10, "Tor 3: ");
      		Ekran.Pisz_XY(3,11, "Tor 4:");
      		Ekran.Pisz_XY(1,15,"+= P-dodaj pociag Q-wyjscie z programu =+");
    	end Tlo; 
        
	procedure Nowy_pociag_godzina is       
     	begin
      		Ekran.Czysc;
      		Ekran.Pisz_XY(1,1,"+=========== Dworzec kolejowy ===========+");
      		Ekran.Pisz_XY(3,5, "Podaj godzine odjazdu nowego pociagu: [0-23]");

      	end Nowy_pociag_godzina;
      
	procedure Nowy_pociag_minuta is       
     	begin
      		Ekran.Czysc;
      		Ekran.Pisz_XY(1,1,"+=========== Dworzec kolejowy ===========+");
      		Ekran.Pisz_XY(3,5, "Podaj minuty odjazdu nowego pociagu: [0-59]");
      	end Nowy_pociag_minuta;      
      
	procedure Nowy_pociag_nazwa is       
     	begin
      		Ekran.Czysc;
      		Ekran.Pisz_XY(1,1,"+=========== Dworzec kolejowy ===========+");
      		Ekran.Pisz_XY(3,5, "Podaj nazwe dla nowego pociagu:");
      	end Nowy_pociag_nazwa;      
      
  end Ekran;
  
  -- ------------------------------------
  -- Zadanie odmierzajace aktualny czas
  -- ------------------------------------
  
  task  czas; 
  task body czas is
  	Nastepny : Ada.Real_Time.Time;
  	Okres : constant Ada.Real_Time.Time_Span := Ada.Real_Time.Milliseconds(1000);
  	Przesuniecie : constant Ada.Real_Time.Time_Span := Ada.Real_Time.Milliseconds(10);
  begin
  	Nastepny := Ada.Real_Time.Clock + Przesuniecie;
  	loop
		delay until Nastepny;
  		Aktualna_Minuta := Aktualna_Minuta+1;		-- zwiekszenie minuty
  		if Aktualna_Minuta > 59 then 			-- zwiekszenie godziny
  			Aktualna_Minuta:= 0;
  			Aktualna_Godzina:=Aktualna_Godzina+1;
  			if Aktualna_Godzina > 23 then 	-- przeskok na nastepny dzien
  				Aktualna_Godzina := 0;
  			end if;
  		end if;
  	
  	if Nie_Rysuj = False then				-- wyswietlenie aktualnej godziny w menu glownym
  		Ekran.Pisz_XY(17,2, "Godz:" & Aktualna_Godzina'Img & ':');
  		if Aktualna_Minuta in 0..9 then Ekran.Pisz_XY(26,2,Aktualna_Minuta'Img);
  		else Ekran.Pisz_XY(25,2,Aktualna_Minuta'Img);
  		end if;
  		Ekran.Pisz_XY(20,16, " "); 			--przesuniecie kursora na dol terminala
  	end if;
  	
  	exit when Koniec = True;
  	Nastepny := Nastepny + Okres;
  end loop;
    
    Ekran.Czysc;
    
    exception
    when E:others => Put_Line("Error: Zadanie generujace zdarzenia");

  

 end czas;
  
  -- ------------------------------------
  -- Zadanie symulujace pociag
  -- ------------------------------------
  

  task type Pociag(Godz: Integer := 0; Min: Integer:= 0);
  type Wsk_Zadanie is access Pociag;
  
  task body Pociag is
  Moja_nazwa: String(1..15) := Nazwa_Pociagu;
  Przydzielony_Peron : Integer := 0;
  Wiersz_konsoli : Integer :=0;
  begin
	Nazwa_Pociagu  := (others => ' ');
	Semafor1.Czekaj;  			--oczekiwanie na mozliwosc wjazdu na dworzec, jezeli liczba pociagow rowna N, to semafor nie zezwoli na wjazd 
		
	-- Przydzial peronu - sprawdzenie ktory z peronow jest wolny
	for i in 1..N loop
		if Perony(i) = False then  -- jezeli peron jest wolny (False) wtedy wjedz na niego
			Perony(i) := True; -- zajecie danego peronu
			Przydzielony_Peron := i;
		end if;
		exit when Przydzielony_Peron /= 0;
	end loop;
		
	-- Przypisanie wiersza w konsoli odpowiadajacego zajetego peronowi
	if Przydzielony_Peron = 1 then Wiersz_konsoli:=6;
	elsif Przydzielony_Peron = 2 then Wiersz_konsoli:=7;
	elsif Przydzielony_Peron = 3 then Wiersz_konsoli:=10;
	elsif Przydzielony_Peron = 4 then  Wiersz_konsoli:=11;
	end if;
		
	--Petla sprawdzajaca czy jest juz osiagnieta godzina odjazdu, dzialajaca co 1s
	loop 
		if Nie_Rysuj = False then Ekran.Pisz_XY(11,Wiersz_konsoli, Godz'Img & ':' & Min'Img & "   " & Moja_nazwa);
		end if;
		
		delay 1.0;
		
		if Godz = Aktualna_Godzina and Min <= Aktualna_Minuta then exit; 	-- odjazd po spelnieniu tych dwoch warunkow
		elsif Godz < Aktualna_Godzina then exit;				-- dodane wskutek testow
		end if;
		exit when Koniec = True; -- zakonczenie pracy zadania w przypadku zakonczenia wykonywania glownego programu
	end loop;
		
	--Zwolnienie peronu
	Perony(Przydzielony_Peron) := False;
	Semafor1.Sygnalizuj;
	Ekran.Pisz_XY(11,Wiersz_konsoli, "                            ");
		
  end Pociag;

  WZX : Wsk_Zadanie; -- utworzenie wskaznika na zadanie

  -- --------------------------------------------------------------------
  -- Procedura do obslugi przypisania godziny odjazdu dla nowego pociagu
  -- --------------------------------------------------------------------
  procedure Nowy_pociag_godz is

  begin 
  	Nie_Rysuj := True; -- zablokowanie rysowania godzin odjazdu i nazw przez taski z pociagami i zegar
  	Ekran.Nowy_pociag_godzina; 

  	loop		
  		begin
  			Get(Godzina); 				-- podanie godziny odjazdu
		 exception
     		 when E:others =>			
			Ekran.Tlo;				--wyswietlenie menu glownego
			Ekran.Pisz_XY(1,14, "Error: Nie podano godziny jako liczby");	--wyswietlenie komunikatu o bledzie
			Nie_Rysuj := False;			-- wylaczenie blokady rysowania godziny w taskach
			Skip_Line;				--wyczyszczenie bufora klawiatury
			exit;					-- wyjscie z procedury Nowy_pociag_godz
		end;
  		
  		if Godzina < 0 or Godzina > 23 then 	-- jezeli podano bledny zakres godziny odjazdu
    			Ekran.Pisz_XY(3,7, "Nie podano poprawnego zakresu. Czy chcesz wrocic do menu glownego? [T/N]");
    			Get_Immediate(Zn);
    			if Zn in 't'|'T' then 		-- powrot do menu glownego
    				Nie_Rysuj := False;	-- odblokowanie rysowania
    				Ekran.Tlo;		--- wyswietlenie glownego menu
    				exit;
    			end if;
    		Ekran.Nowy_pociag_godzina; -- jezeli wcisnieto inny znak niz T|t to powrot do ekranu z nowa godzina
    		
  		elsif Godzina >= 0 and Godzina < 24 then --jezeli podano wlasciwy zakres przejdz do procedury przypisania minut
  		Nowy_pociag_min;
  		exit;
  	end if;
    	

    	end loop;

  end Nowy_pociag_godz;
  
  -- ----------------------------------------------------------------------
  -- Procedura do obslugi przypisania minut odjazdu dla nowego pociagu
  -- ----------------------------------------------------------------------
  procedure Nowy_pociag_min is
  begin
  	
  	Ekran.Nowy_pociag_minuta;
  	
   	loop
   		 begin
  			Get(Minuta); 				-- podanie minut odjazdu
		 exception
     		 when E:others =>			
			Ekran.Tlo;
			Ekran.Pisz_XY(1,14, "Error: Nie podano minut jako liczby");
			Nie_Rysuj := False;
			Skip_Line;	
			exit;
		end;
   		
   		if Minuta < 0 or Minuta > 59 then	-- jezeli podano bledny zakres minut odjazdu
    			Ekran.Pisz_XY(3,7, "Nie podano poprawnego zakresu. Czy chcesz wrocic do menu glownego? [T/N]");
    			Get_Immediate(Zn);
    		    	if Zn in 't'|'T' then		-- powrot do menu glownego
    				Nie_Rysuj := False;
    				Ekran.Tlo;
    				exit;
    			end if;
    		Ekran.Nowy_pociag_minuta; 
    		 
   		elsif Minuta >= 0 and Minuta <= 59 then
   			Nowy_pociag_naz; 		-- przejscie do procedury z nazwa pociagu
   			exit;
  		end if;
  	end loop;
  end Nowy_pociag_min;
  
    -- ----------------------------------------------------------------------
  -- Procedura do obslugi przypisania nazwy dla nowego pociagu
  -- ----------------------------------------------------------------------
  
  procedure Nowy_pociag_naz is
  begin
	Ekran.Nowy_pociag_nazwa;
   	loop
   		Get_Line(Nazwa_pociagu,Dlugosc_nazwy_pociagu); 	-- wczytanie z klawiatury nazwy pociagu
   		if not (Dlugosc_nazwy_pociagu = 0) then
   			WZX := new Pociag(Godzina, Minuta);		-- utworzenie nowego zadania - pociagu
   			Nie_Rysuj := False; 				-- zwolnienie blokady od wyswietlania godzin odjazdow pociagu
 			exit;
 		end if;
 	end loop;
	
	Dlugosc_nazwy_pociagu := 0;
    	Ekran.Tlo; 
  end Nowy_pociag_naz;
  
  
  -- -------------------------------------
  -- 		Procedura główna
  -- -------------------------------------
  
begin
  -- narysowanie tła menu glownego
  Ekran.Tlo; 
  
  --glowna petla
  loop
    
    Get_Immediate(Zn); -- wczytanie znaku z klawiatury
    exit when Zn in 'q'|'Q'; -- jezeli q|Q to wyjdz z programu
    
   if Zn in 'p'| 'P' then -- jezeli p|P to uruchomienie procedury przypisania godziny odjazdu pociagu
	Nowy_Pociag_godz;
   end if;
	Ekran.Pisz_XY(1,18, "PG");
  end loop; --petla glowna
   	
  Ekran.Czysc; -- wyczyszczenie konsoli terminala
  Koniec := True;

end Dworzec;    
